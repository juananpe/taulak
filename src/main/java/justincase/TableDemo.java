package justincase;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import hasiera.MyTableModel;

public class TableDemo extends JPanel {

	public TableDemo() {
		super(new GridLayout(1,0));
		MyTableModel mtm = new MyTableModel();
		mtm.kargatu();
		JTable table = new JTable(mtm);
		
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
	}

}
