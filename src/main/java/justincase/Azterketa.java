package justincase;

import javax.swing.JFrame;

public class Azterketa {

	private static void createAndShowGUI(){
		JFrame frame = new JFrame("Azterketa 2016/2017");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		AzterketaDemo newContentPane = new AzterketaDemo();
		newContentPane.setOpaque(true);
		frame.setContentPane(newContentPane);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
