package justincase;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import hasiera.Lag;
import hasiera.MyTableModel;

public class AzterketaDemo extends JPanel {

	public AzterketaDemo() {
		super(new GridLayout(1,2));
		
		MyTableModel modelo1 = new MyTableModel();
		modelo1.kargatu();
		MyTableModel modelo2 = new MyTableModel();
		
		// Hobe: new JTable eta ondoren taula.kargatu() behar denean
		JTable table1 = new JTable(modelo1);
		JTable table2 = new JTable(modelo2);
		
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		JButton sartu = new JButton("Sartu");
		sartu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table1.getRowCount() == 0)
					return;
				
				int selectedRow = table1.getSelectedRow();
				if (selectedRow < 0)
					selectedRow = table1.getRowCount()-1;
				
				Lag lerroa = modelo1.lortu(selectedRow);
				modelo1.remove(selectedRow);
				modelo2.sartu(lerroa);
				modelo1.fireTableDataChanged();
				modelo2.fireTableDataChanged();
				
			}
		});
		panel.add(sartu);
		
		JButton kendu = new JButton("Kendu");
		kendu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table2.getRowCount() == 0)
					return;
				
				int selectedRow = table2.getSelectedRow();
				if (selectedRow < 0)
					selectedRow = table2.getRowCount()-1;
				
				Lag lerroa = modelo2.lortu(selectedRow);
				modelo2.remove(selectedRow);
				modelo1.sartu(lerroa);
				modelo2.fireTableDataChanged();
				modelo1.fireTableDataChanged();
				
			}
		});
		panel.add(kendu);
		
		JButton gorde = new JButton("Gorde");
		gorde.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				modelo2.gorde();
			}
		});
		panel.add(gorde);
		
		
		
		JScrollPane scrollPane1 = new JScrollPane(table1);
		JScrollPane scrollPane2 = new JScrollPane(table2);
		add(scrollPane1);
		add(panel);
		add(scrollPane2);
	}

}
