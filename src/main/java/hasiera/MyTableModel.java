package hasiera;

import java.awt.Image;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import hasiera.Lag;

public class MyTableModel extends AbstractTableModel {

	private List<Lag> data = new Vector<Lag>();
	private Vector<String> columnNames = new Vector<String>();

	public MyTableModel() {
		hasieratuZutabeIzenak();
	}
	
	@Override
	public int getRowCount() {
		
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		return data.get(rowIndex).getBalioa(columnIndex);
	}
	
	public String getColumnName(int colIndex){
		return columnNames.get(colIndex);
	}
	
	public void hasieratuZutabeIzenak(){
		columnNames.add("First Name");
		columnNames.add("Last Name");
		columnNames.add("Sport");
		columnNames.add("# of Years");
		columnNames.add("Vegetarian");
		columnNames.add("Argazkia");
		
	}
	
	public Class<?> getColumnClass(int col){
		switch (col) {
		case 0:
		case 1:
		case 2:
				return String.class;
		case 3:
			return Integer.class;
		case 4: 
			return Boolean.class;
		case 5: 
			return ImageIcon.class;
		}
		return null;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	};
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		data.get(rowIndex).insertElementAt(value, columnIndex);
	};
	
	public void kargatu(){
		
		/*
		ImageIcon imageIcon = new ImageIcon(new ImageIcon("pics/person.png").getImage().getScaledInstance(-1, 50, Image.SCALE_DEFAULT));
		data.add(new Lag("Kathy", "Smith", "Snowboarding", 5, false, imageIcon));
		data.add(new Lag("John", "Doe", "Rowing", 3, true, new ImageIcon("pics/person.png")));
		data.add(new Lag("Sue", "Black", "Knitting", 2, true, new ImageIcon("pics/person.png")));
		data.add(new Lag("Jane", "White", "Speed reading", 20, true, new ImageIcon("pics/person.png")));
		*/
		data = DatuKud.instantzia.datuakLortu();
		
		
	}

	public void gorde(){
		DatuKud.instantzia.datuakGorde(data);
	}
	
	public static void main(String[] args) {
		MyTableModel taula = new MyTableModel();
		taula.kargatu();
		System.out.println("Lerroak:" + taula.getRowCount());
		System.out.println("Zutabeak:" + taula.getColumnCount());
		System.out.println("(2,2) elementuaren balioa:" + taula.getValueAt(2, 2));
		System.out.println("Lehenengo zutabearen izena:" + taula.getColumnName(0));
	}

	public void gehituLerroa() {
		data.add(new Lag("", "", "", 0, true, "pics/person.png"));
		fireTableStructureChanged();
	}

	public int ezabatuLerroa(int selectedRow) {
		if (selectedRow != -1){
			data.remove(selectedRow);
			fireTableStructureChanged();
		}
		return data.size();
	}

	public Lag lortu(int selectedRow) {
		// TODO Auto-generated method stub
		return data.get(selectedRow);
	}

	public void sartu(Lag lerroa) {
		data.add(lerroa);
		
	}

	public void remove(int selectedRow) {
		data.remove(selectedRow);
	}
}
