package hasiera;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatuKud {

	// singleton patroia
	public static DatuKud instantzia = new DatuKud();
	
	private DatuKud() {
		
	}

		
	public List<Lag> datuakLortu(){
		List<Lag> emaitza = new ArrayList<Lag>();
		DBKudeatzaile dbk = DBKudeatzaile.getInstantzia();
		String query = "select firstname, lastname, sport, numyears, vegetarian from datuak";
		ResultSet rs =	dbk.execSQL(query);
		try {
			while (rs.next()){
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String sport = rs.getString("sport");
				Integer numyears = rs.getInt("numyears");
				Boolean vegetarian = rs.getBoolean("vegetarian");
				emaitza.add(new Lag(firstname,lastname,sport, numyears, vegetarian));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return emaitza;
	}


	public void datuakGorde(List<Lag> data) {
		DBKudeatzaile dbk = DBKudeatzaile.getInstantzia();
		dbk.execSQL("delete from datuak;");
		for (Lag lag : data) {
			String query = "insert into datuak ('firstname', 'lastname','sport','numyears','vegetarian','argazkia') "
					+ " values (" + lag.toQuery() + ")";
			System.out.println(query);
			dbk.execSQL(query);
		}
		
	}
	
}
