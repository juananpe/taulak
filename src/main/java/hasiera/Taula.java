package hasiera;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Taula extends JFrame {

	public Taula() {
		
		super("Nire taula grafikoa");
		final int ALTUERA = 50;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MyTableModel mtm = new MyTableModel();
		mtm.kargatu();
		JTable table = new JTable(mtm);
		table.setRowHeight(ALTUERA);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel botoiak = new JPanel();
		BoxLayout bl = new BoxLayout(botoiak,BoxLayout.X_AXIS);
		botoiak.setLayout(bl);
		
		JButton txertatuBotoia = new JButton("Txertatu");
		txertatuBotoia.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mtm.gehituLerroa();
			}
		});
		botoiak.add(txertatuBotoia);
		
		
		JButton ezabatuBotoia = new JButton("Ezabatu");
		ezabatuBotoia.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int last = mtm.ezabatuLerroa(table.getSelectedRow());
				table.setRowSelectionInterval(last-1, last-1);
			}
		});
		botoiak.add(ezabatuBotoia);
		
		
		add(botoiak, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new Taula();
	}
}
