package hasiera;

import java.awt.Image;
import java.net.URL;
import java.util.Vector;

import javax.swing.ImageIcon;

public class Lag {
	String izena;
	String abizena;
	String kirola;
	Integer tamaina;
	Boolean baiez;
	ImageIcon ikurra;

	public Lag(String izena, String abizena, String kirola, Integer tamaina, Boolean baiez) {
		this(izena, abizena, kirola, tamaina, baiez, "person.png");
	}

	public Lag(String izena, String abizena, String kirola, Integer tamaina, Boolean baiez, String ikurra) {
		super();
		this.izena = izena;
		this.abizena = abizena;
		this.kirola = kirola;
		this.tamaina = tamaina;
		this.baiez = baiez;

		URL path = this.getClass().getClassLoader().getResource(ikurra);
		// System.out.println("Path" + path);

		Image irudia = new ImageIcon(path).getImage();
		ImageIcon irudiaIcon = new ImageIcon(irudia.getScaledInstance(-5, 50, java.awt.Image.SCALE_SMOOTH));
		this.ikurra = irudiaIcon;
	}

	public String toQuery() {
		return "'" + izena + "', '" + abizena + "', '" + kirola + "', '" + tamaina + "', '" + baiez + "', '" + ikurra
				+ "'";
	}

	@Override
	public String toString() {
		return "Lag [izena=" + izena + ", abizena=" + abizena + ", kirola=" + kirola + ", tamaina=" + tamaina
				+ ", baiez=" + baiez + "]";
	}

	public Object getBalioa(int i) {
		Object emaitza = null;
		switch (i) {
		case 0:
			emaitza = izena;
			break;
		case 1:
			emaitza = abizena;
			break;
		case 2:
			emaitza = kirola;
			break;
		case 3:
			emaitza = tamaina;
			break;
		case 4:
			emaitza = baiez;
			break;
		case 5:
			emaitza = ikurra;
			break;
		default:
			break;
		}

		return emaitza;
	}

	public void insertElementAt(Object value, int i) {
		switch (i) {
		case 0:
			izena = (String) value;
			break;
		case 1:
			abizena = (String) value;
			break;
		case 2:
			kirola = (String) value;
			break;
		case 3:
			tamaina = (Integer) value;
			break;
		case 4:
			baiez = (Boolean) value;
			break;
		case 5:
			ikurra = (ImageIcon) value;
			break;
		default:
			break;
		}
	}

	public static void main(String[] args) {
		Vector<Lag> data = new Vector<Lag>();
		data.add(new Lag("Kathy", "Smith", "Snowboarding", 5, false, "person.png"));
		data.add(new Lag("John", "Doe", "Rowing", 3, true, "person.png"));
		data.add(new Lag("Sue", "Black", "Knitting", 2, true, "person.png"));
		data.add(new Lag("Jane", "White", "Speed reading", 20, true, "person.png"));

		System.out.println(data);

		Lag lerro0 = data.get(0);
		lerro0.insertElementAt("Sierra", 1);
		System.out.println("Lehenengo lerroko bigarren atributuaren balioa (0,1):" + lerro0.getBalioa(1));

		Lag lerro3 = data.get(3);
		lerro3.insertElementAt(new Integer(4), 3);
		System.out.println("Azken lerroko laugarren atributuaren balioa (3,3):" + lerro3.getBalioa(3));

	}
}